# My business Finder

Site permettant de Rechercher des entreprises grâce à l'API SIRÈNE via une interface web.

## Framework utilisés 

Laravel, Tailwind

## Languages utilisés 

Php, html(blade), css, js

## Informations supplémentaires

J'ai eu un petit soucis au niveau de l'api, étant en deux liens différents, j'ai repris les deux json afin de n'en faire qu'une seule et même liste sans specification de recherche, je l'ai ensuite convertie en sql.

Je me suis permis de faire une petite présentation en acceuil afin de rendre la visite plus agréable, j'espère que ça vous plaira ! 