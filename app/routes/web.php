<?php

use App\Http\Controllers\EntreprisesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('accueil');
})->name('accueil');

Route::get('/entreprises', [
    EntreprisesController::class,
    'list'
])->name('entreprises');

Route::get('/entreprises/{id}',[
    EntreprisesController::class,
    'detail'
])->name('entrepriseDetail');

Route::get('/ajouter', function () {
    return view('ajouterEntreprise');
})->name('ajouter');
