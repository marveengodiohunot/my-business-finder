<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Entreprises;

class EntreprisesController extends Controller
{
    public function list(){
        $entreprises = DB::table('entreprises')->paginate(3);

        return view("entreprises",['entreprises' => $entreprises]);
    }

    public function detail($id){
        $entreprise = DB::table('entreprises')->where("id", $id)->first();
        return view('entrepriseDetail', ['entreprise' => $entreprise]);
    }

    public function search(){
        $search_text= $_GET['query'];
        $entreprise = DB::table('entreprises')->where('l1_normalisee','LIKE','%'.$search_text.'%')->get();
        return view('entreprises');
    }
}
