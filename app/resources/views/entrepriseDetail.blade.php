@extends('layouts.layout')

@section('title', 'Detail')


@section('content')

<div class="flex text-center justify-center ">
    <div class="flex flex-row flex-wrap p-3">
        <div class="rounded-lg shadow-lg p-6 bg-white">
            <div class="mx-auto md:w-2/3 sm:w-2/5">
                <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                    <div class="lg:text-center">
                        <h2 class="text-3xl text-white font-light inline rounded px-4 py-2 tracking-wide uppercase bg-gray-900">{{$entreprise->l1_normalisee}}</h2>
                        <p class="mt-4 max-w-2xl text-xl text-gray-500 lg:mx-auto">
                        {{$entreprise->libelle_activite_principale}}
                        </p>
                    </div>

                    <div class="mt-10">
                        <dl class="space-y-10 md:space-y-0 md:grid md:grid-cols-2 md:gap-x-8 md:gap-y-10">
                            <div class="relative">
                                <dt>
                                    <h4 class="absolute flex items-center justify-center w-20 h-8 rounded-md  tracking-wide font-regular text-sm text-blue-500">
                                        SIREN
                                    </h4>
                                    <h5 class="pt-1 ml-24 text-lg leading-6 font-medium text-gray-900">{{$entreprise->siren}}</h5>
                                </dt>
                            </div>
                            <div class="relative">
                                <dt>
                                    <h4 class="absolute flex items-center justify-center w-20 h-8 rounded-md  tracking-wide font-regular text-sm text-blue-500">
                                        SIRET
                                    </h4>
                                    <h5 class="pt-1 ml-24 text-lg leading-6 font-medium text-gray-900">{{$entreprise->siret}}</h5>
                                </dt>
                            </div>
                            <div class="relative">
                                <dt>
                                    <h4 class="absolute flex items-center justify-center w-20 h-8 rounded-md  tracking-wide font-regular text-sm text-blue-500">
                                        Adresse Complète
                                    </h4>
                                    <h5 class="pt-1 ml-24 text-md leading-6 font-medium text-gray-900">{{$entreprise->geo_adresse}}</h5>
                                </dt>
                            </div>
                            <div class="relative">
                                <dt>
                                    <h4 class="absolute flex items-center justify-center w-20 h-8 rounded-md  tracking-wide font-regular text-sm text-blue-500">
                                        Libelé juridique
                                    </h4>
                                    <h5 class="pt-1 ml-24 text-md leading-6 font-medium text-gray-900">{{$entreprise->libelle_nature_juridique_entreprise}}</h5>
                                </dt>
                            </div>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop