@extends('layouts.layout')

@section('title', 'Home')


@section('content')
    <div class="grid justify-center container mx-auto max-w-5xl">
        <!--Left Col-->
        <div class="flex flex-col text-center ">
            <div class="flex flex-row flex-wrap p-3">
                <div class="mx-auto md:w-2/3 sm:w-2/5">
                    <!-- Profile Card -->
                    <div class="flex justify-center ">
                        <img src="img/logo.png">
                    </div>
                    <h1 class="mb-10 text-4xl font-bold leading-tight ">Bienvenue sur My Business Finder</h1>
                    <div class="rounded-lg shadow-lg p-6 bg-white">
                        <div>
                            <p class="text-lg tracking-loose w-full">Ici trouver une entreprise n'est qu'une formalité
                                !</p>
                            <p class="text-lg tracking-loose w-full leading-normal">Facile à prendre en main, il vous
                                est possible de trouver une entreprise sans vous prendre la
                                tête!</p>
                        </div>
                        <div class="justify-center mt-10">
                            <button
                                class="mx-auto lg:mx-0 hover:underline bg-white text-gray-800 md:font-bold rounded-full py-4 px-8 shadow-xl">
                                <a href="{{'entreprises'}}" class="flex items-center justify-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                                         stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                              d="M15 15l-2 5L9 9l11 4-5 2zm0 0l5 5M7.188 2.239l.777 2.897M5.136 7.965l-2.898-.777M13.95 4.05l-2.122 2.122m-5.657 5.656l-2.12 2.122"/>
                                    </svg>
                                    <p class="ml-2">Commencer les recherches !</p>
                                </a>
                            </button>
                        </div>
                    </div>
                    <div class="rounded-lg shadow-lg p-6 bg-white mt-10">
                        <div class="flex items-center justify-center text-green-400">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-20 w-20" fill="none"
                                 viewBox="0 0 24 24"
                                 stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                      d="M9.663 17h4.673M12 3v1m6.364 1.636l-.707.707M21 12h-1M4 12H3m3.343-5.657l-.707-.707m2.828 9.9a5 5 0 117.072 0l-.548.547A3.374 3.374 0 0014 18.469V19a2 2 0 11-4 0v-.531c0-.895-.356-1.754-.988-2.386l-.548-.547z"/>
                            </svg>
                        </div>
                        <div>
                            <p>Nous sommes entrain de créer une nouvelle fonctionalité vous permetant de
                                proposer
                                l'ajout
                                d'une
                                entreprise à notre répertoire ! <br> N'est-ce pas magnifique !</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop
