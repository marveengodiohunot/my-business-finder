@extends('layouts.layout')

@section('title', '404')


@section('content')
    <div class="grid justify-center container mx-auto ">
        <!--Left Col-->
        <div class="flex flex-col text-center ">
            <div class="max-w-lg">
                <div class="rounded-lg shadow-lg p-6 bg-white mt-10">
                    <p>Oh mince, la page n'est pas encore arrivée sur notre site !</p>
                    <p class="mb-5">J'espère qu'elle sera bientôt la !</p>
                    <img src="https://media.giphy.com/media/l46CbZ7KWEhN1oci4/giphy.gif" alt="this slowpoke moves"/>
                </div>
            </div>

            <div class="justify-center mt-10">
                <button
                    class="mx-auto lg:mx-0 hover:underline bg-white text-gray-800 md:font-bold rounded-full py-4 px-8 shadow-xl">
                    <a href="{{'/'}}" class="flex items-center justify-center">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                             stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                  d="M15 15l-2 5L9 9l11 4-5 2zm0 0l5 5M7.188 2.239l.777 2.897M5.136 7.965l-2.898-.777M13.95 4.05l-2.122 2.122m-5.657 5.656l-2.12 2.122"/>
                        </svg>
                        <p class="ml-2">Revenir à l'accueil</p>
                    </a>
                </button>
            </div>
        </div>
    </div>
@stop
